# Biller

### GET "/billers"

#### headers:

"Content-Type" : "application/json"


#### params:
- "page" - start with 1
- "count" - count of billers
- "needAll" - if true, then ignore other params

#### response: 
array of biller, and count of all billers

#### response sample:

```json
{
  "count": 3,
  "billers": [
	{
	  "id": 3,
	  "name": "Awesome biller 3"
	},
	{
	  "id": 2,
	  "name": "Awesome biller 2"
	},
	{
	  "id": 1,
	  "name": "Awesome biller"
	}
  ]
}
```

### POST "/billers"

#### headers:

"Content-Type" : "application/json"

#### request body sample:

```json
{
  "name": "Awesome biller"
}
```
#### response sample:

```json
{
  "id": 1,
  "name": "Awesome biller"
}
```

### PUT "/billers/{biller-id}"

#### headers:

"Content-Type" : "application/json"

#### path variables:
biller id

#### request body sample:

```json
{
  "name": "Awesome biller 2"
}
```
#### response sample:
```json
{
  "id": 1,
  "name": "Awesome biller 2"
}
```

### DELETE "/billers/{biller-id}"

#### headers:

"Content-Type" : "application/json"

#### path variables:

biller id

#### response status: 200


# Customer


### GET "/customer"

#### headers:

"Content-Type" : "application/json"

#### params:
- "page" - start with 1
- "count" - count of billers
- "needAll" - if true, then ignore other params


#### response: 

array of customer, and count of all customers

#### response sample:

```json
{
  "count": 3,
  "customers": [
	{
	  "id": 3,
	  "firstname": "Firstname",
	  "lastname": "Lastname",
	  "address": "Some adress, 256 char max",
	  "dateOfBirth": 1513620333048
	},
	{
	  "id": 2,
	  "firstname": "Firstname",
	  "lastname": "Lastname",
	  "address": "Some adress, 256 char max",
	  "dateOfBirth": 1513620333048
	},
	{
	  "id": 1,
	  "firstname": "Firstname",
	  "lastname": "Lastname",
	  "address": "Some adress, 256 char max",
	  "dateOfBirth": 1513620333048
	}
  ]
}
```


### POST "/customers"

#### headers:

"Content-Type" : "application/json"

#### request body sample:

```json
{
  "firstname": "Firstname",
  "lastname": "Lastname",
  "address": "Some adress, 256 char max",
  "dateOfBirth": 1513620333128
}
```

#### response sample:

```json
{
  "id": 1,
  "firstname": "Firstname",
  "lastname": "Lastname",
  "address": "Some adress, 256 char max",
  "dateOfBirth": 1513620333128
}
```

### PUT "/customers/{customer-id}"

#### headers:

"Content-Type" : "application/json"

#### path variables:

customer id

#### request body sample:

```json
{
  "firstname": "Firstname",
  "lastname": "Lastname",
  "address": "Some adress, 256 char maxa",
  "dateOfBirth": 1513620333128
}
```

#### response sample:

```json
{
  "id": 1,
  "firstname": "Firstname",
  "lastname": "Lastname",
  "address": "Some adress, 256 char maxa",
  "dateOfBirth": 1513620333128
}
```

### DELETE "/customers/{customer-id}"

#### headers:

"Content-Type" : "application/json"

#### path variables:

customer id

#### response status: 200



# Payment

### GET "/payments"

#### headers:

"Content-Type" : "application/json"

#### params:
- "page" - start with 1
- "count" - count of billers
- "needAll" - if true, then ignore other params
- customerId
- billerId

#### response:

```json
{
  "count": 3,
  "payments": [
	{
	  "id": 1,
	  "date": 1513620333089,
	  "biller": {
		"id": 3,
		"name": "Awesome biller 3"
	  },
	  "customer": {
		"id": 3,
		"firstname": "Firstname",
		"lastname": "Lastname",
		"address": "Some adress, 256 char max",
		"dateOfBirth": 1513620333089
	  },
	  "account": 123213,
	  "amount": 100.0
	},
	{
	  "id": 2,
	  "date": 1513620333089,
	  "biller": {
		"id": 2,
		"name": "Awesome biller 2"
	  },
	  "customer": {
		"id": 2,
		"firstname": "Firstname",
		"lastname": "Lastname",
		"address": "Some adress, 256 char max",
		"dateOfBirth": 1513620333089
	  },
	  "account": 123213,
	  "amount": 100.0
	},
	{
	  "id": 3,
	  "date": 1513620333089,
	  "biller": {
		"id": 1,
		"name": "Awesome biller"
	  },
	  "customer": {
		"id": 1,
		"firstname": "Firstname",
		"lastname": "Lastname",
		"address": "Some adress, 256 char max",
		"dateOfBirth": 1513620333089
	  },
	  "account": 123213,
	  "amount": 100.0
	}
  ]
}
```

### POST "/payments"

#### headers:

"Content-Type" : "application/json"

#### request body:

```json
{
  "billerId": 1,
  "customerId": 1,
  "account": 123213,
  "amount": 100.7
}
```

#### response:

```json
{
  "id": 1,
  "date": 1513620333128,
  "biller": {
	"id": 1,
	"name": "Awesome biller"
  },
  "customer": {
	"id": 1,
	"firstname": "Firstname",
	"lastname": "Lastname",
	"address": "Some adress, 256 char max",
	"dateOfBirth": 1513620333128
  },
  "account": 123213,
  "amount": 100.7
}
```