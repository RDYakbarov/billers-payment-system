package com.payment.system.bill.controllers;

import com.payment.system.bill.dtos.BillersDto;
import com.payment.system.bill.models.Biller;
import com.payment.system.bill.services.BillersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;


@RestController
public class BillerController {
	@Autowired
	private BillersService billersService;

	@GetMapping("/billers")
	public BillersDto getBillers(@RequestParam(name = "page", required = false) Integer page,
	                             @RequestParam(name = "count", required = false) Integer count,
	                             @RequestParam(name = "needAll", required = false) Boolean needAll) {
		return billersService.getBillers(count, page, needAll);
	}

	@PostMapping("/billers")
	@ResponseStatus(CREATED)
	public Biller saveBiller(@RequestBody Biller biller) {
		return billersService.save(biller);
	}

	@PutMapping("/billers/{biller-id}")
	public Biller updateBiller(@PathVariable("biller-id") Long billerId, @RequestBody Biller biller) {
		return billersService.update(billerId, biller);
	}

	@DeleteMapping("/billers/{biller-id}")
	@ResponseStatus(OK)
	public void deleteBiller(@PathVariable("biller-id") Long billerId) {
		billersService.delete(billerId);
	}
}
