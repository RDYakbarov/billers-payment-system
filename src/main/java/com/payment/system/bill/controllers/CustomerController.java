package com.payment.system.bill.controllers;

import com.payment.system.bill.dtos.CustomersDto;
import com.payment.system.bill.models.Customer;
import com.payment.system.bill.services.CustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;


@RestController
public class CustomerController {
	@Autowired
	private CustomersService customersService;

	@GetMapping("/customers")
	public CustomersDto getCustomers(@RequestParam(name = "page", required = false) Integer page,
	                                 @RequestParam(name = "count", required = false) Integer count,
	                                 @RequestParam(name = "needAll", required = false) Boolean needAll) {
		return customersService.getCustomers(count, page, needAll);
	}

	@PostMapping("/customers")
	@ResponseStatus(CREATED)
	public Customer saveCustomer(@RequestBody Customer customer) {
		return customersService.save(customer);
	}

	@PutMapping("/customers/{customer-id}")
	public Customer updateCustomer(@PathVariable("customer-id") Long customerId, @RequestBody Customer customer) {
		return customersService.update(customerId, customer);
	}

	@DeleteMapping("/customers/{customer-id}")
	@ResponseStatus(OK)
	public void deleteCustomer(@PathVariable("customer-id") Long customerId) {
		customersService.delete(customerId);
	}
}
