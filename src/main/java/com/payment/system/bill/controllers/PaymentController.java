package com.payment.system.bill.controllers;

import com.payment.system.bill.dtos.PaymentDto;
import com.payment.system.bill.dtos.PaymentsDto;
import com.payment.system.bill.models.Payment;
import com.payment.system.bill.services.PaymentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;


@RestController
public class PaymentController {
	@Autowired
	private PaymentsService paymentsService;

	@GetMapping("/payments")
	public PaymentsDto getPayments(@RequestParam(name = "page", required = false) Integer page,
	                                @RequestParam(name = "count", required = false) Integer count,
	                                @RequestParam(name = "customerId", required = false) Long customerId,
	                                @RequestParam(name = "billerId", required = false) Long billerId,
	                                @RequestParam(name = "needAll", required = false) Boolean needAll) {
		return paymentsService.getPayments(customerId, billerId, count, page, needAll);
	}

	@PostMapping("/payments")
	@ResponseStatus(CREATED)
	public Payment savePayment(@RequestBody PaymentDto paymentDto) {
		return paymentsService.save(paymentDto);
	}
}
