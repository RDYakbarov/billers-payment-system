package com.payment.system.bill.dtos;

import com.payment.system.bill.models.Biller;
import lombok.Builder;
import lombok.Getter;

import java.util.List;


@Builder
@Getter
public class BillersDto {
	private Long count;
	private List<Biller> billers;
}
