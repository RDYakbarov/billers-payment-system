package com.payment.system.bill.dtos;

import com.payment.system.bill.models.Customer;
import com.payment.system.bill.models.Payment;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@Builder
public class CustomersDto {
	private Long count;
	private List<Customer> customers;
}
