package com.payment.system.bill.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Builder
public class PaymentDto {
	private Long billerId;
	private Long customerId;
	private Long account;
	private Double amount;
}
