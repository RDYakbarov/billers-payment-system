package com.payment.system.bill.repositories;

import com.payment.system.bill.models.Biller;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface BillersRepository extends PagingAndSortingRepository <Biller, Long> {
	List<Biller> findAll();
}
