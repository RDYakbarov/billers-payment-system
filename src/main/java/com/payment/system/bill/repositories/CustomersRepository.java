package com.payment.system.bill.repositories;

import com.payment.system.bill.models.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


public interface CustomersRepository extends PagingAndSortingRepository <Customer, Long> {
	List<Customer> findAll();
}
