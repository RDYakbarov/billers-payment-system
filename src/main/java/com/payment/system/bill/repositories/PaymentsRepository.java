package com.payment.system.bill.repositories;

import com.payment.system.bill.models.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


public interface PaymentsRepository extends PagingAndSortingRepository<Payment, Long> {
	List<Payment> findAll();

	Page<Payment> findAllByBiller_Id(Long billerId, Pageable pageable);

	Page<Payment> findAllByCustomer_Id(Long customerId, Pageable pageable);

	Page<Payment> findAllByCustomer_IdAndBiller_Id(Long customerId, Long billerId, Pageable pageable);

	List<Payment> findAllByBiller_Id(Long billerId);

	List<Payment> findAllByCustomer_Id(Long customerId);

	List<Payment> findAllByCustomer_IdAndBiller_Id(Long customerId, Long billerId);

	long countByCustomer_Id(Long customerId);

	long countByBiller_Id(Long billerId);

	long countByCustomer_IdAndBiller_Id(Long customerId, Long billerId);
}
