package com.payment.system.bill.services;

import com.payment.system.bill.dtos.BillersDto;
import com.payment.system.bill.models.Biller;
import com.payment.system.bill.models.Payment;


public interface BillersService {
	BillersDto getBillers(Integer count, Integer page, Boolean needAll);

	Biller save(Biller biller);

	Biller update(Long billerId, Biller biller);

	void delete(Long billerId);
}
