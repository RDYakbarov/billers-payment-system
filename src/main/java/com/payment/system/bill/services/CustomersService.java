package com.payment.system.bill.services;

import com.payment.system.bill.dtos.CustomersDto;
import com.payment.system.bill.models.Customer;
import com.payment.system.bill.models.Payment;


public interface CustomersService {
	CustomersDto getCustomers(Integer count, Integer page, Boolean needAll);

	Customer save(Customer customer);

	Customer update(Long customerId, Customer customer);

	void delete(Long customerId);
}
