package com.payment.system.bill.services;

import com.payment.system.bill.dtos.PaymentDto;
import com.payment.system.bill.dtos.PaymentsDto;
import com.payment.system.bill.models.Payment;


public interface PaymentsService {
	PaymentsDto getPayments(Long customerId, Long billerId, Integer count, Integer page, Boolean needAll);

	Payment save(PaymentDto payment);
}
