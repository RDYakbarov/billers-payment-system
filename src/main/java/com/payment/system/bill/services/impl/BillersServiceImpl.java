package com.payment.system.bill.services.impl;

import com.payment.system.bill.dtos.BillersDto;
import com.payment.system.bill.exceptions.NotFoundException;
import com.payment.system.bill.models.Biller;
import com.payment.system.bill.repositories.BillersRepository;
import com.payment.system.bill.services.BillersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BillersServiceImpl implements BillersService {
	@Value("${default.page.size}")
	private int defaultCount;
	@Value("${default.page}")
	private int defaultPage;
	@Autowired
	private BillersRepository billerRepository;

	@Override
	public BillersDto getBillers(Integer count, Integer page, Boolean needAll) {
		List<Biller> billers = needAll != null && needAll ? billerRepository.findAll()
				: billerRepository.findAll(createPageRequest(count, page)).getContent();
		return BillersDto.builder()
				.billers(billers)
				.count(billerRepository.count())
				.build();
	}

	@Override
	public Biller save(Biller biller) {
		return billerRepository.save(biller);
	}

	@Override
	public Biller update(Long billerId, Biller biller) {
		Biller prevBiller = billerRepository.findOne(billerId);
		if (prevBiller == null) throw new NotFoundException("Biller with this id not Found");
		copyNonNull(prevBiller, biller);
		return billerRepository.save(prevBiller);
	}

	@Override
	public void delete(Long billerId) {
		Biller prevBiller = billerRepository.findOne(billerId);
		if (prevBiller == null) throw new NotFoundException("Biller with this id not Found");
		billerRepository.delete(billerId);
	}

	private Pageable createPageRequest(Integer count, Integer page) {
		count = count == null || count < 0 ? defaultCount : count;
		page = page == null || page < 1 ? defaultPage : page;
		return new PageRequest(--page, count);
	}

	private void copyNonNull(Biller prevBiller, Biller biller) {
		if (biller.getName() != null) prevBiller.setName(biller.getName());
	}
}
