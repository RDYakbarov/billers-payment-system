package com.payment.system.bill.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.payment.system.bill.dtos.CustomersDto;
import com.payment.system.bill.exceptions.NotFoundException;
import com.payment.system.bill.models.Customer;
import com.payment.system.bill.repositories.CustomersRepository;
import com.payment.system.bill.services.CustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomersServiceImpl implements CustomersService {
	@Value("${default.page.size}")
	private int defaultCount;
	@Value("${default.page}")
	private int defaultPage;
	@Autowired
	private CustomersRepository customerRepository;

	@Override
	public CustomersDto getCustomers(Integer count, Integer page, Boolean needAll) {
		List<Customer> customers = needAll != null && needAll ? customerRepository.findAll()
				: customerRepository.findAll(createPageRequest(count, page)).getContent();
		return CustomersDto.builder()
				.customers(customers)
				.count(customerRepository.count())
				.build();
	}

	@Override
	public Customer save(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer update(Long customerId, Customer customer) {
		Customer prevCustomer = customerRepository.findOne(customerId);
		if (prevCustomer == null) throw new NotFoundException("Customer with this id not Found");
		copyNonNull(prevCustomer, customer);
		return customerRepository.save(prevCustomer);
	}

	@Override
	public void delete(Long customerId) {
		Customer prevCustomer = customerRepository.findOne(customerId);
		if (prevCustomer == null) throw new NotFoundException("Customer with this id not Found");
		customerRepository.delete(customerId);
	}

	private Pageable createPageRequest(Integer count, Integer page) {
		count = count == null || count < 0 ? defaultCount : count;
		page = page == null || page < 1 ? defaultPage : page;
		return new PageRequest(--page, count);
	}

	private void copyNonNull(Customer prevCustomer, Customer customer) {
		if (customer.getFirstname() != null) prevCustomer.setFirstname(customer.getFirstname());
		if (customer.getLastname() != null) prevCustomer.setLastname(customer.getLastname());
		if (customer.getDateOfBirth() != null) prevCustomer.setDateOfBirth(customer.getDateOfBirth());
		if (customer.getAddress() != null) prevCustomer.setAddress(customer.getAddress());
	}
}
