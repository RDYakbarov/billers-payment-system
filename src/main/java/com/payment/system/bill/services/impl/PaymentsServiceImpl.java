package com.payment.system.bill.services.impl;

import com.payment.system.bill.dtos.PaymentDto;
import com.payment.system.bill.dtos.PaymentsDto;
import com.payment.system.bill.exceptions.BadRequestException;
import com.payment.system.bill.models.Biller;
import com.payment.system.bill.models.Customer;
import com.payment.system.bill.models.Payment;
import com.payment.system.bill.repositories.BillersRepository;
import com.payment.system.bill.repositories.CustomersRepository;
import com.payment.system.bill.repositories.PaymentsRepository;
import com.payment.system.bill.services.PaymentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;


@Service
public class PaymentsServiceImpl implements PaymentsService {
	@Value("${default.page.size}")
	private int defaultCount;
	@Value("${default.page}")
	private int defaultPage;

	@Autowired
	private PaymentsRepository paymentsRepository;
	@Autowired
	private BillersRepository billersRepository;
	@Autowired
	private CustomersRepository customersRepository;

	@Override
	public PaymentsDto getPayments(Long customerId, Long billerId, Integer count, Integer page, Boolean needAll) {
		List<Payment> payments;
		long resCount;
		if (customerId != null) {
			if (billerId != null) {
				payments = needAll != null && needAll ? paymentsRepository.findAllByCustomer_IdAndBiller_Id(customerId, billerId)
						: paymentsRepository.findAllByCustomer_IdAndBiller_Id(customerId, billerId, createPageRequest(count, page)).getContent();
				resCount = paymentsRepository.countByCustomer_IdAndBiller_Id(customerId, billerId);
			} else {
				payments = needAll != null && needAll ? paymentsRepository.findAllByCustomer_Id(customerId)
						: paymentsRepository.findAllByCustomer_Id(customerId, createPageRequest(count, page)).getContent();
				resCount = paymentsRepository.countByCustomer_Id(customerId);
			}
		} else {
			if (billerId != null) {
				payments = needAll != null && needAll ? paymentsRepository.findAllByBiller_Id(billerId)
						: paymentsRepository.findAllByBiller_Id(billerId, createPageRequest(count, page)).getContent();
				resCount = paymentsRepository.countByBiller_Id(billerId);
			} else {
				payments = needAll != null && needAll ? paymentsRepository.findAll()
						: paymentsRepository.findAll(createPageRequest(count, page)).getContent();
				resCount = paymentsRepository.count();
			}
		}
		return PaymentsDto.builder()
				.payments(payments)
				.count(resCount)
				.build();
	}

	@Override
	public Payment save(PaymentDto paymentDto) {
		validate(paymentDto);
		Customer customer = customersRepository.findOne(paymentDto.getCustomerId());
		Biller biller = billersRepository.findOne(paymentDto.getBillerId());
		return paymentsRepository.save(Payment.builder()
				.account(paymentDto.getAccount())
				.amount(paymentDto.getAmount())
				.customer(customer)
				.biller(biller)
				.date(new Timestamp(System.currentTimeMillis()))
				.build());
	}

	private void validate(PaymentDto paymentDto) {
		if (paymentDto.getAccount() == null)
			throw new BadRequestException("Expected in body field Account");
		else if (paymentDto.getAmount() == null)
			throw new BadRequestException("Expected in body field Amount");
		else if (paymentDto.getBillerId() == null)
			throw new BadRequestException("Expected in body field Biller");
		else if (paymentDto.getCustomerId() == null)
			throw new BadRequestException("Expected in body field Customer");
		else if (!customersRepository.exists(paymentDto.getCustomerId()))
			throw new BadRequestException("Biller with id " + paymentDto.getCustomerId() + "not found");
		else if (!billersRepository.exists(paymentDto.getBillerId()))
			throw new BadRequestException("Biller with id " + paymentDto.getBillerId() + "not found");

	}

	private Pageable createPageRequest(Integer count, Integer page) {
		count = count == null || count < 0 ? defaultCount : count;
		page = page == null || page < 1 ? defaultPage : page;
		return new PageRequest(--page, count);
	}
}
